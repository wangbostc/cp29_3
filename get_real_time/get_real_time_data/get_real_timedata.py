from __future__ import print_function
import json
import base64
import hmac
import hashlib
import time
from threading import Thread
from websocket import create_connection, WebSocketConnectionClosedException
from pymongo import MongoClient
from gdax.order_book import OrderBook



if __name__ == '__main__':
    import sys
    import time
    import datetime as dt


    class Realtime_data(OrderBook):

        def __init__(self, product_id=None):
            super(Realtime_data, self).__init__(product_id=product_id)

            self._bid = None
            self._ask = None
            self._bid_depth = None
            self._ask_depth = None

        def on_message(self, message):
            super(Realtime_data, self).on_message(message)

            bid = self.get_bid()
            bids = self.get_bids(bid)
            bid_depth = sum([b['size'] for b in bids])
            ask = self.get_ask()
            asks = self.get_asks(ask)
            ask_depth = sum([a['size'] for a in asks])
            if self._bid == bid and self._ask == ask and self._bid_depth == bid_depth and self._ask_depth == ask_depth:
                pass
            else:
                self._bid = bid
                self._ask = ask
                self._bid_depth = bid_depth
                self._ask_depth = ask_depth

                '''here is the bid(self._bid,slef._ask,self._bid_depth,self_ask_depth) ask we can save it into mongodb'''
                print('{} {} bid: {:.3f} @ {:.2f}\task: {:.3f} @ {:.2f}'.format(
                    dt.datetime.now(), self.product_id, bid_depth, bid, ask_depth, ask))

    realtime_data = Realtime_data()
    realtime_data.start()
    try:
        while True:
            time.sleep(10)
    except KeyboardInterrupt:
        realtime_data.close()

    if realtime_data.error:
        sys.exit(1)
    else:
        sys.exit(0)