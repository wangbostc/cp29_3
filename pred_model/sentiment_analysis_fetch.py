#!/usr/bin/python

# This codes is used to fetch BTC related tweets from twitter for every minutes (change sleep time)
# analyse polarities (-1 to 1) of all fetched tweets and calculate the mean
# the mean for each time interval would act as extra features for prediction of BTC price

import tweepy
import time
import csv
import numpy as np
from textblob import TextBlob
from datetime import datetime


def fetch_data():
    """
    use to fetch text data from twitter api
    :return:
    """

    # set twitter api credentials

    consumer_key = 'x7hP0m9t8p5kLszU5u2uY4QJX'
    consumer_secret = 'kHZ4SXRzClQH9QFX7ANA0qGlfYkYeksNufZtsPyuyWgSDDYWHs'

    access_token = '141519857-LkDIGYjGFiSX4q06g9ERjDKUk39dgpcFwAMkRO2S'
    access_token_secret = 'SmOwIchA5AV0RHrrUsrKPRYUgCdQWf0tvc7jMEOdXctgu'

    # use OAuth Authentication to access twitter api
    # refer to http://docs.tweepy.org/en/v3.6.0/auth_tutorial.html
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)

    # open a csv file to save the data
    csv_file = open("twitter_sentiment.csv", 'a')
    writer = csv.writer(csv_file, delimiter=',')

    while True:

        # fetch tweets by keywords
        BTC_tweets = api.search(q=['bitcoin'], count=10000, since='2018-05-15')

        # get polarity mean
        mean = get_polarity_mean(BTC_tweets)
        print(mean)

        # attach write time
        data = [mean, datetime.now().strftime('%Y/%m/%d %H:%M:%S')]
        print(data)

        #save data
        writer.writerow(data)
        csv_file.flush()

        # time.sleep(60)


def get_polarity_mean(BTC_tweets):
    """

    :param BTC_tweets: Bitcoin tweets
    :return: mean of po
    """
    polarities = []

    for tweet in BTC_tweets:

        analysis = TextBlob(tweet.text)
        polarities.append(analysis.sentiment.polarity)

    return np.mean(polarities)


if __name__ == "__main__":
    fetch_data()
