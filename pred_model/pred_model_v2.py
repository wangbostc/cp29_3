#!/usr/bin/python

import pandas as pd
import numpy as np
import matplotlib.pylab as plt
from time import time
from math import sqrt
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout, Activation


def dataset_setup(dataset1=None, dataset2=None, look_back=None, sentiment=False, n=None):
    """
    """
    data_X = list()
    data_y = list()

    for i in range(len(dataset1) - look_back - 1 - n):
        price = dataset1[i:i + look_back, 0].tolist()
        if sentiment:
            price.append(dataset2[i])
        data_X.append(price)
        data_y.append(dataset1[i + look_back + n, 0])

    return np.array(data_X), np.array(data_y)


def dataset_reshape(data_X):
    return np.reshape(data_X, (data_X.shape[0], data_X.shape[1], 1))


def dataset_gen(dataset1=None, dataset2=None, look_back=10, train_test_split=0.85, start_data=None,
                sentiment=False, n=None):
    """
    Use for data-extraction (JSON to pandas df?)
    input data from sentiment analysis
    Including the train/test data splitting
    :return:
    """

    train_size = int(len(dataset1) * train_test_split)
    train_set, test_set = dataset1[0:train_size, :], dataset1[train_size:, :]
    # print(len(train_set), len(test_set), len(dataset1))

    train_X, train_y = dataset_setup(dataset1=train_set, dataset2=dataset2, look_back=look_back, n=n)
    test_X, test_y = dataset_setup(dataset1=test_set, dataset2=dataset2, look_back=look_back, n=n)

    train_X = dataset_reshape(train_X)
    test_X = dataset_reshape(test_X)
    print(train_X.shape)
    print(train_y.shape)
    return train_X, train_y, test_X, test_y, train_set, test_set


def train_model(train_X, train_Y, test_X=None, test_y=None, states=80, epochs=None, batch_size=None, verbose=2,
                dropout=0.2, shuffle=False):
    """
    ML model: LSTM
    :return:
    """

    model = Sequential()
    # model.add(LSTM(states, batch_input_shape=(32, train_X.shape[1], train_X.shape[2]), return_sequences=True,
    #                stateful=False))
    model.add(LSTM(states, input_shape=(train_X.shape[1], train_X.shape[2]), return_sequences=True))
    model.add(Dropout(dropout))  # drop out regulation to first layer
    # model.add(LSTM(states, return_sequences=True, stateful=True))
    model.add(LSTM(states, stateful=False))
    model.add(Dropout(dropout))  # drop out regulation to second layer
    model.add(Dense(1))  # output file size
    model.add(Activation('tanh'))  # active function
    model.compile(loss='mse', optimizer='adam')

    # for i in range(epochs):
    model.fit(train_X, train_Y, batch_size=batch_size, epochs=epochs, verbose=verbose, shuffle=shuffle)
        # model.reset_states()

    return model


def prediction(model, train_X, n_pred):
    """
    prediction
    :param model:
    :param text_X:
    :return:
    """
    # data = train_X[-1, 1, :].reshape(1, 1, 60)
    # for i in range(n_pred):


def feature_engineering(df):
    """
    preprocess data for better result
    :return:
    """


def accuracy_cal(true_data, prediction, steps_num=None):
    true_dif = []
    pred_dif = []
    for i in range(1, len(true_data)):
        true_dif.append(true_data[i]-true_data[i-1])
        pred_dif.append(prediction[i]-predictions[i-1])
    return np.sum(np.sign(np.array(true_dif)) == np.sign(np.array(pred_dif)))/steps_num


if __name__ == "__main__":

    # load price data
    df = pd.read_json('historic_rates.json', lines=True)
    df1 = df[:5000]

    # only take the closing price and reverse the order (the newest one in the bottom)
    df1 = df1[['close']].iloc[::-1]
    dataset1 = df1.values.astype('float32')

    # scale the data to be in range of -1 to 1 and thus could be fit in tanh activation function
    mms = MinMaxScaler(feature_range=(-1, 1))
    dataset1 = mms.fit_transform(dataset1)

    # read sentiment analysis data
    df = pd.read_csv('twitter_sentiment.csv', header=None)
    dataset2 = df.iloc[:, 0]  # first column represents the mean tweets polarities per minute

    # ---------------------------------------------------------------------------------------#
    #           multiple steps prediction with independent value prediction                 #
    # ---------------------------------------------------------------------------------------#
    # this method predicts each step individually with a separated model
    # advantage: error accumulation could be avoided
    # disadvantage: longer training time due to multiple models required

    # define the number of steps to predict
    steps_to_predict = 20
    train_test_split = 0.85
    look_back = 10
    train_score = []
    test_score = []
    predictions = []

    # extract test data
    train_X, *_, train_set, test_set = dataset_gen(dataset1=dataset1, dataset2=dataset2, look_back=look_back,
                                                   train_test_split=train_test_split, n=0)

    # data used to predict comes from the last window of features (size depends on lookback)
    pred_data = train_X[-1:]

    # true data contains the last train data point + number of steps into the prediction data
    true_data = np.insert(test_set[:steps_to_predict], 0, train_set[-1][0])
    print(true_data)
    for i in range(steps_to_predict):
        # generate proper 3D input for LSTM model training
        train_X, train_y, test_X, test_y, *_ = dataset_gen(dataset1=dataset1, dataset2=dataset2, look_back=look_back,
                                                           train_test_split=train_test_split, n=i)
        # print(train_X[:5], train_y[:5])

        # train model with generated dataset
        model = train_model(train_X, train_y, test_X, test_y, epochs=1, batch_size=256)
        # evaluate the model
        train_score.append(model.evaluate(train_X, train_y))
        test_score.append(model.evaluate(test_X, test_y))
        predictions.append(model.predict(pred_data)[-1:][0])

    # inverse transfer the data
    true_data = mms.inverse_transform(true_data.reshape(-1, 1))
    predictions = mms.inverse_transform(predictions)

    # print results
    print("Average Training MSE:", np.average(train_score))
    print("Average Test MSE:", np.average(test_score))
    print(true_data)
    predictions = np.insert(predictions, 0, true_data[0])
    print(predictions)
    accuracy = accuracy_cal(true_data, predictions, steps_to_predict)
    print("===========", accuracy, "=============")
    plt.plot(true_data, label='Actual Data')
    plt.plot(predictions, label='Predictions')
    plt.legend(loc='best')
    plt.show()
    exit()


    # train_X, train_y, test_X, test_y = dataset_gen(dataset1=dataset1, dataset2=dataset2)
    # score, pred_train, pred_test = pred_model(train_X, train_y, test_X, test_y)
    # test_pred_inv = mms.inverse_transform(pred_test.reshape(1, -1))
    # test_y_inv = mms.inverse_transform(test_y.reshape(1, -1))
    # mse_test = sqrt(mean_squared_error(test_y_inv, test_pred_inv))
    # print('Test RMSE: %.3f' % mse_test)

    # test drop-out
    # drop_out = np.arange(0.0, 0.55, 0.05)
    # dropout_list = list()
    # for i in drop_out:
    #     score, pred_train, pred_test, history_callback = pred_model(train_X, train_y, test_X, test_y, dropout=i)
    #     test_pred_inv = mms.inverse_transform(pred_test.reshape(1, -1))
    #     test_y_inv = mms.inverse_transform(test_y.reshape(1, -1))
    #     mse_test = sqrt(mean_squared_error(test_y_inv, test_pred_inv))
    #     print('Test RMSE: %.3f' % mse_test)
    #     dropout_list.append(mse_test)
    # print(dropout_list)
    # plt.figure(figsize=(8, 8))
    # plt.plot(drop_out, dropout_list)
    # plt.xlabel('Percent of dropout', fontsize=20)
    # plt.ylabel('MSE', fontsize=20)
    # plt.title('MSE vs dropout', fontsize=24)
    # plt.savefig("drop_out.png", dpi=300)

    # test look_back
    look_back_list = list()
    for i in range(1, 2):
        train_X, train_y, test_X, test_y = dataset_gen(dataset1=dataset1, dataset2=dataset2, look_back=1)
        score, model, history_callback = train_model(train_X, train_y, test_X, test_y)

        test_pred_inv = mms.inverse_transform(model.predict(test_X, batch_size=1).reshape(1, -1))
        test_y_inv = mms.inverse_transform(test_y.reshape(1, -1))
        mse_test = sqrt(mean_squared_error(test_y_inv, test_pred_inv))
        print('Test RMSE: %.3f' % mse_test)
        look_back_list.append(mse_test)
    print(look_back_list)
    plt.figure(figsize=(8, 8))
    plt.plot([i for i in range(1, 2)], look_back_list)
    plt.xlabel('No. of look_back', fontsize=20)
    plt.ylabel('MSE', fontsize=20)
    plt.title('MSE vs look_back', fontsize=24)
    plt.savefig("look_back.png", dpi=300)

    # no of states test
    # states_para = dict()
    # states_list = list()
    # for i in range(1, 111, 5):
    #     t0 = time()
    #     score, pred_train, pred_test, history_callback = pred_model(train_X, train_y, test_X, test_y, states=i)
    #     t = time() - t0
    #     print(score)
    #     test_pred_inv = mms.inverse_transform(pred_test.reshape(1, -1))
    #     test_y_inv = mms.inverse_transform(test_y.reshape(1, -1))
    #     mse_test = sqrt(mean_squared_error(test_y_inv, test_pred_inv))
    #     print('Test RMSE: %.3f' % mse_test)
    #     states_list.append(mse_test)
    #     states_para[i] = (mse_test, t)
    # print(states_para)
    # plt.figure(figsize=(8, 8))
    # plt.plot([i for i in range(1, 111, 5)], states_list)
    # plt.xlabel('No. of states', fontsize=20)
    # plt.ylabel('MSE', fontsize=20)
    # plt.title('MSE vs states', fontsize=24)
    # plt.savefig("states.png", dpi=300)

    # no of epoches test
    # epoch = 100
    # score, pred_train, pred_test, history_callback = pred_model(train_X, train_y, test_X, test_y, epochs=epoch)
    # test_pred_inv = mms.inverse_transform(pred_test.reshape(1, -1))
    # test_y_inv = mms.inverse_transform(test_y.reshape(1, -1))
    # mse_test = sqrt(mean_squared_error(test_y_inv, test_pred_inv))
    # loss_history = history_callback.history["loss"]
    # print(loss_history)
    # print('Test RMSE: %.3f' % mse_test)
    # plt.figure(figsize=(8, 8))
    # plt.plot(list(range(epoch)), loss_history)
    # plt.xlabel('No. of epochs', fontsize=20)
    # plt.ylabel('loss', fontsize=20)
    # plt.title('MSE vs epochs', fontsize=24)
    # plt.savefig("epochs.png", dpi=300)

    # no of batches test
    # batch_para = dict()
    # batch_list = list()
    # batch = [2 ** i for i in range(4, 9)]
    # for i in batch:
    #     print(i)
    #     t0 = time()
    #     score, pred_train, pred_test,  history_callback = pred_model(train_X, train_y, test_X, test_y, batch_size=i)
    #     t = time() - t0
    #     # print(score)
    #     test_pred_inv = mms.inverse_transform(pred_test.reshape(1, -1))
    #     test_y_inv = mms.inverse_transform(test_y.reshape(1, -1))
    #     mse_test = sqrt(mean_squared_error(test_y_inv, test_pred_inv))
    #     print('Test RMSE: %.3f' % mse_test)
    #     batch_list.append(mse_test)
    #     batch_para[i] = (mse_test, t)
    # print(batch_para)
    # plt.figure(figsize=(8, 8))
    # plt.plot(batch, batch_list)
    # plt.xlabel('No. of batches', fontsize=20)
    # plt.ylabel('MSE', fontsize=20)
    # plt.title('MSE vs batches', fontsize=24)
    # plt.savefig("batches.png", dpi=300)

    # train_pred = mms.inverse_transform(pred_train.reshape(1, -1))
    # train_y = mms.inverse_transform(train_y.reshape(1, -1))
